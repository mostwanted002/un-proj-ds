#!/usr/bin/env python3
import requests
import json
import time

banner = """---------------United Nations Comtrade Data Extraction--------------------"""

file = open('comtrade.json','a')
print(banner)

def sortIt(e):
    return(e['TradeValue'])


retries = 0
availabilityCheckUrl = 'https://comtrade.un.org/api/refs/da/view'
baseURL = 'http://comtrade.un.org/api/get'
availabilityParams = {'freq':'A', 'ps':'', 'type':'C', 'px':'HS'}
availableCountries = []
years = ['2008','2009','2010','2011','2012','2013','2014','2015','2016','2017','2018','2019','2020']
countryList = {'Afghanistan': '4', 'Albania': '8', 'Algeria': '12', 'Andorra': '20', 'Angola': '24', 'Anguilla': '660', 'Antigua and Barbuda': '28', 'Argentina': '32', 'Armenia': '51', 'Aruba': '533', 'Australia': '36', 'Austria': '40', 'Azerbaijan': '31', 'Bahamas': '44', 'Bahrain': '48', 'Bangladesh': '50', 'Barbados': '52', 'Belarus': '112', 'Belgium': '56', 'Belgium-Luxembourg': '58', 'Belize': '84', 'Benin': '204', 'Bermuda': '60', 'Bhutan': '64', 'Bolivia (Plurinational State of)': '68', 'Bonaire': '535', 'Bosnia Herzegovina': '70', 'Botswana': '72', 'Br. Virgin Isds': '92', 'Brazil': '76', 'Brunei Darussalam': '96', 'Bulgaria': '100', 'Burkina Faso': '854', 'Burundi': '108', 'Cabo Verde': '132', 'Cambodia': '116', 'Cameroon': '120', 'Canada': '124', 'Cayman Isds': '136', 'Central African Rep.': '140', 'Chad': '148', 'Chile': '152', 'China': '156', 'China, Hong Kong SAR': '344', 'China, Macao SAR': '446', 'Colombia': '170', 'Comoros': '174', 'Congo': '178', 'Cook Isds': '184', 'Costa Rica': '188', "Côte d'Ivoire": '384', 'Croatia': '191', 'Cuba': '192', 'Curaçao': '531', 'Cyprus': '196', 'Czechia': '203', 'Czechoslovakia': '200', "Dem. People's Rep. of Korea": '408', 'Dem. Rep. of the Congo': '180', 'Denmark': '208', 'Djibouti': '262', 'Dominica': '212', 'Dominican Rep.': '214', 'East and West Pakistan': '588', 'Ecuador': '218', 'Egypt': '818', 'El Salvador': '222', 'Equatorial Guinea': '226', 'Eritrea': '232', 'Estonia': '233', 'Ethiopia': '231', 'EU-28': '97', 'Faeroe Isds': '234', 'Falkland Isds (Malvinas)': '238', 'Fiji': '242', 'Finland': '246', 'Fmr Arab Rep. of Yemen': '886', 'Fmr Dem. Rep. of Germany': '278', 'Fmr Dem. Rep. of Vietnam': '866', 'Fmr Dem. Yemen': '720', 'Fmr Ethiopia': '230', 'Fmr Fed. Rep. of Germany': '280', 'Fmr Pacific Isds': '582', 'Fmr Panama, excl.Canal Zone': '590', 'Fmr Panama-Canal-Zone': '592', 'Fmr Rep. of Vietnam': '868', 'Fmr Rhodesia Nyas': '717', 'Fmr Sudan': '736', 'Fmr Tanganyika': '835', 'Fmr USSR': '810', 'Fmr Yugoslavia': '890', 'Fmr Zanzibar and Pemba Isd': '836', 'France': '251', 'French Guiana': '254', 'French Polynesia': '258', 'FS Micronesia': '583', 'Gabon': '266', 'Gambia': '270', 'Georgia': '268', 'Germany': '276', 'Ghana': '288', 'Gibraltar': '292', 'Greece': '300', 'Greenland': '304', 'Grenada': '308', 'Guadeloupe': '312', 'Guatemala': '320', 'Guinea': '324', 'Guinea-Bissau': '624', 'Guyana': '328', 'Haiti': '332', 'Holy See (Vatican City State)': '336', 'Honduras': '340', 'Hungary': '348', 'Iceland': '352', 'India': '699', 'India, excl. Sikkim': '356', 'Indonesia': '360', 'Iran': '364', 'Iraq': '368', 'Ireland': '372', 'Israel': '376', 'Italy': '381', 'Jamaica': '388', 'Japan': '392', 'Jordan': '400', 'Kazakhstan': '398', 'Kenya': '404', 'Kiribati': '296', 'Kuwait': '414', 'Kyrgyzstan': '417', "Lao People's Dem. Rep.": '418', 'Latvia': '428', 'Lebanon': '422', 'Lesotho': '426', 'Liberia': '430', 'Libya': '434', 'Lithuania': '440', 'Luxembourg': '442', 'Madagascar': '450', 'Malawi': '454', 'Malaysia': '458', 'Maldives': '462', 'Mali': '466', 'Malta': '470', 'Marshall Isds': '584', 'Martinique': '474', 'Mauritania': '478', 'Mauritius': '480', 'Mayotte': '175', 'Mexico': '484', 'Mongolia': '496', 'Montenegro': '499', 'Montserrat': '500', 'Morocco': '504', 'Mozambique': '508', 'Myanmar': '104', 'N. Mariana Isds': '580', 'Namibia': '516', 'Nepal': '524', 'Neth. Antilles': '530', 'Neth. Antilles and Aruba': '532', 'Netherlands': '528', 'New Caledonia': '540', 'New Zealand': '554', 'Nicaragua': '558', 'Niger': '562', 'Nigeria': '566', 'Norway': '579', 'Oman': '512', 'Other Asia, nes': '490', 'Pakistan': '586', 'Palau': '585', 'Panama': '591', 'Papua New Guinea': '598', 'Paraguay': '600', 'Peninsula Malaysia': '459', 'Peru': '604', 'Philippines': '608', 'Poland': '616', 'Portugal': '620', 'Qatar': '634', 'Rep. of Korea': '410', 'Rep. of Moldova': '498', 'Réunion': '638', 'Romania': '642', 'Russian Federation': '643', 'Rwanda': '646', 'Ryukyu Isd': '647', 'Sabah': '461', 'Saint Helena': '654', 'Saint Kitts and Nevis': '659', 'Saint Kitts, Nevis and Anguilla': '658', 'Saint Lucia': '662', 'Saint Maarten': '534', 'Saint Pierre and Miquelon': '666', 'Saint Vincent and the Grenadines': '670', 'Samoa': '882', 'San Marino': '674', 'Sao Tome and Principe': '678', 'Sarawak': '457', 'Saudi Arabia': '682', 'Senegal': '686', 'Serbia': '688', 'Serbia and Montenegro': '891', 'Seychelles': '690', 'Sierra Leone': '694', 'Singapore': '702', 'Slovakia': '703', 'Slovenia': '705', 'So. African Customs Union': '711', 'Solomon Isds': '90', 'Somalia': '706', 'South Africa': '710', 'South Sudan': '728', 'Spain': '724', 'Sri Lanka': '144', 'State of Palestine': '275', 'Sudan': '729', 'Suriname': '740', 'Swaziland': '748', 'Sweden': '752', 'Switzerland': '757', 'Syria': '760', 'Tajikistan': '762', 'TFYR of Macedonia': '807', 'Thailand': '764', 'Timor-Leste': '626', 'Togo': '768', 'Tokelau': '772', 'Tonga': '776', 'Trinidad and Tobago': '780', 'Tunisia': '788', 'Turkey': '792', 'Turkmenistan': '795', 'Turks and Caicos Isds': '796', 'Tuvalu': '798', 'Uganda': '800', 'Ukraine': '804', 'United Arab Emirates': '784', 'United Kingdom': '826', 'United Rep. of Tanzania': '834', 'Uruguay': '858', 'US Virgin Isds': '850', 'USA': '842', 'USA (before 1981)': '841', 'Uzbekistan': '860', 'Vanuatu': '548', 'Venezuela': '862', 'Viet Nam': '704', 'Wallis and Futuna Isds': '876', 'Yemen': '887', 'Zambia': '894', 'Zimbabwe': '716', 'ASEAN': '975'}
params = {'r':'','freq':'A','ps':'','p':'0','px':'HS','type':'C','rg':''}
totalParams = {'r':'','freq':'A','ps':'','p':'0','px':'HS','type':'C','cc':'TOTAL'}
fields = ['yr','rtTitle','rgDesc','cmdDescE','TradeValue']

def getTotal(country, year):
    global retries
    totalParams['ps'] = year
    totalParams['r'] = countryList[country]
    totalImportTrade = 'NA'
    totalExportTrade = 'NA'
    try:
        con = requests.get(baseURL, totalParams)
        totalTradeData = json.loads(con.text)['dataset']
        con.close()
        if len(totalTradeData) >=2:
            totalImportTrade = totalTradeData[0]['TradeValue']
            totalExportTrade = totalTradeData[1]['TradeValue']
        else:
            totalImportTrade = totalTradeData[0]['TradeValue']
    except Exception as e:
        print("Error: At total Trade Value for {}\n".format(country),e)
        time.sleep(30)
        if(retries <= 3):
            retries += 1
            totalImportTrade, totalExportTrade = getTotal(country, year)
        else:
            retries=0
        pass
    return(totalImportTrade, totalExportTrade)

def getTop5Imports(country, year):
    global retries
    top5 = []
    params['ps'] = year
    params['r'] = countryList[country]
    params['rg'] = '1'
    try:
        con = requests.get(baseURL, params)
        top5 = json.loads(con.text)['dataset']
        con.close()
        top5.sort(key=sortIt,reverse=True)
        if len(top5)>5:
            top5 = top5[0:5]
    except Exception as e:
        print("Error: At total Top5Import Value for\n".format(country),e)
        time.sleep(30)
        if(retries<=3):
            retries += 1
            top5 = getTop5Imports(country, year)
        else:
            retries = 0
        pass
    return(top5)

def getTop5Exports(country, year):
    global retries
    top5 = []
    params['ps'] = year
    params['r'] = countryList[country]
    params['rg'] = '2'
    try:
        con = requests.get(baseURL, params)
        top5 = json.loads(con.text)['dataset']
        con.close()
        top5.sort(key=sortIt,reverse=True)
        if len(top5)>5:
                top5 = top5[0:5]
    except Exception as e:
        print("Error: At total Top5Export Value for\n".format(country),e)
        time.sleep(30)
        if(retries<=3):
            retries+=1
            top5 = getTop5Exports(country, year)
        else:
            retries = 0
        pass
    return(top5)

for year in years:
    finalData = {year:{}}
    availableCountries = []
    try:
        print("Checking available Trade Partners for Year: {}".format(str(year)))
        availabilityParams['ps'] = year
        con = requests.get(availabilityCheckUrl, availabilityParams)
        resp = json.loads(con.text)
        print("Found data for {} Trade Partners.".format(len(resp)))
        con.close()
        for i in range(len(resp)):
            countryName = resp[i]['rDesc']
            if countryName in countryList:
                availableCountries.append(countryName)
        time.sleep(10)
        for country in availableCountries:
            print("Fetching data for {}".format(country))
            totalImportTrade, totalExportTrade = getTotal(country, year)
            print("Total Export for year {} : {}".format(year, totalExportTrade))
            print("Total Import for year {} : {}".format(year, totalImportTrade))
            time.sleep(10)
            top5 = getTop5Imports(country, year)
            topFiveImportCommodities = []
            for commodity in top5:
                temp = {}
                temp.update({'commodity':commodity['cmdDescE']})
                temp.update({'TradeValue':commodity['TradeValue']})
                topFiveImportCommodities.append(temp)
                del temp
            time.sleep(10)
            top5 = getTop5Exports(country, year)
            if len(top5)>5:
                top5 = top5[0:5]
            topFiveExportCommodities = []
            for commodity in top5:
                temp = {}
                temp.update({'commodity':commodity['cmdDescE']})
                temp.update({'TradeValue':commodity['TradeValue']})
                topFiveExportCommodities.append(temp)
                del temp
            thisCountryData = {"import":[totalImportTrade, topFiveImportCommodities], "export":[totalExportTrade, topFiveExportCommodities]}
            finalData[year].update({country:thisCountryData})
            print("Fetch successful for {}".format(country))
            time.sleep(300)
        file.write(json.dumps(finalData)+'\n')
    except Exception as e:
        file.write(json.dumps(finalData)+'\n')
        print(e)
        pass
    del availableCountries
    del finalData